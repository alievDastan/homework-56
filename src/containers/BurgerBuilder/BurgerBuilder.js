import React, {Component} from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import './BurgerBuilder.css'

class BurgerBuilder extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ingredients: [
                {name: 'salad', label: 'Salad', qty: 0, price: 5, disable: true},
                {name: 'bacon', label: 'Bacon', qty: 0, price: 30, disable: true},
                {name: 'cheese', label: 'Cheese', qty: 0, price: 20, disable: true},
                {name: 'meat', label: 'Meat', qty: 0, price: 50, disable: true},
            ],
            totalPrice: 20,
            isModalOpen: false
        }

        this.addItems = (type) => {
            const updatedIngredients = this.state.ingredients.map(ingredient => {
                if (ingredient.name === type) {
                    ingredient.qty++;
                    ingredient.disable = false;
                    this.state.totalPrice = this.state.totalPrice + ingredient.price;
                }
                return ingredient;
            });

            this.setState({
                ingredients: [].concat(updatedIngredients),
            })
        }

        this.removeItems = (type) => {
            const updatedIngredients = this.state.ingredients.map(ingredient => {
                if (ingredient.name === type) {
                    if (ingredient.qty > 0) {
                        ingredient.qty--;
                        ingredient.disable = false;
                        this.state.totalPrice = this.state.totalPrice - ingredient.price;
                    }
                    if (ingredient.qty === 0) {
                        ingredient.disable = true;
                    }
                }
                return ingredient;
            });

            this.setState({
                ingredients: [].concat(updatedIngredients),
            })
        }
    }

    render() {
        return (
            <div>
                <Toolbar/>
                <div className="contentBlock">
                    <div className="burgerBlock">
                        <Burger ingredients={this.state.ingredients}/>
                    </div>
                    <div className="builderBlock">
                        <BuildControls
                            removeItems={this.removeItems}
                            addItems={this.addItems}
                            totalPrice={this.state.totalPrice}
                            ingredients={this.state.ingredients}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

export default BurgerBuilder;
