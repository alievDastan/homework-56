import React from 'react';

const layout = (props) => {
    return (
        <div className='Content'>
            <main>{props.children}</main>
        </div>
    );
}

export default layout;
