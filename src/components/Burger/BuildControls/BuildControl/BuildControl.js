import React from 'react';
import './BuildControl.css';

const buildControl = (props) => {

    return (
        <div className='BuildControl'>
            <div className='Label'>{props.label}</div>
            <button disabled={props.disable} className="lessBtn" onClick={props.removeItems}>-</button>
            <button className='moreBtn' onClick={props.addItems}>+</button>
        </div>
    )
}

export default buildControl;
